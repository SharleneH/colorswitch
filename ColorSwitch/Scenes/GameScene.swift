//
//  GameScene.swift
//  ColorSwitch
//
//

import SpriteKit

enum PlayColors {
    static let colors = [UIColor(red: 254/255, green: 254/255, blue: 97/255, alpha: 1)
                         ,UIColor(red: 240/255, green: 190/255, blue: 65/255, alpha: 1)
                         ,UIColor(red: 237/255, green: 157/255, blue: 56/255, alpha: 1)
                         ,UIColor(red: 234/255, green: 96/255, blue: 44/255, alpha: 1)
                         ,UIColor(red: 233/255, green: 65/255, blue: 43/255, alpha: 1)
                         ,UIColor(red: 154/255, green: 41/255, blue: 75/255, alpha: 1)
                         ,UIColor(red: 122/255, green: 22/255, blue: 169/255, alpha: 1)
                         ,UIColor(red: 56/255, green: 6/255, blue: 157/255, alpha: 1)
                         ,UIColor(red: 28/255, green: 69/255, blue: 244/255, alpha: 1)
                         ,UIColor(red: 64/255, green: 144/255, blue: 201/255, alpha: 1)
                         ,UIColor(red: 120/255, green: 174/255, blue: 71/255, alpha: 1)
                         ,UIColor(red: 212/255, green: 233/255, blue: 86/255, alpha: 1),]
}

enum SwitchSatae: Int {
    case colorOne , colorTwo , colorThree, colorFour, colorFive, colorSix, colorSeven, colorEight, colorNine, colorTen, colorElevent, colorTwelve
}

class GameScene: SKScene {
    
    var colorSwitch: SKSpriteNode!
    var switchState = SwitchSatae.colorOne
    var currentColorIndex : Int?
    
    let scoreLabel = SKLabelNode(text: "0")
    var score = 0
    override func didMove(to view: SKView) {
        setupPhysics()
        layoutScene()
    }
    
    func setupPhysics(){
        physicsWorld.gravity = CGVector(dx: 0.0, dy: -0.5)
        physicsWorld.contactDelegate = self
        
    }
    
    func layoutScene(){
        backgroundColor = UIColor(red: 44/255, green: 62/255, blue: 80/255, alpha: 1.0)
        colorSwitch = SKSpriteNode(imageNamed: "img_colorCircle")
        colorSwitch.size = CGSize(width: frame.size.width/3, height: frame.size.width/3)
        colorSwitch.position = CGPoint(x: frame.midX, y: frame.minY + colorSwitch.size.height)
        colorSwitch.zPosition = ZPositions.colorSwitch
        colorSwitch.physicsBody = SKPhysicsBody(circleOfRadius: colorSwitch.size.width/2)
        colorSwitch.physicsBody?.categoryBitMask = PhysicsCategories.switchCategory
        colorSwitch.physicsBody?.isDynamic = false
        addChild(colorSwitch)
        
        scoreLabel.fontName = "AvenirNext-Bold"
        scoreLabel.fontSize = 60.0
        scoreLabel.fontColor = .white
        scoreLabel.position = CGPoint(x: frame.midX, y: frame.midY)
        scoreLabel.zPosition = ZPositions.label
        addChild(scoreLabel)
        
        spawnBall()
    }
    
    func updateScoreLabel(){
        scoreLabel.text = "\(score)"
    }
    
    func spawnBall(){
        currentColorIndex = Int(arc4random_uniform(UInt32(12)))
        let ball = SKSpriteNode(texture: SKTexture(imageNamed: "img_whiteBall"), color: PlayColors.colors[currentColorIndex!], size: CGSize(width: 30, height: 30))
        ball.colorBlendFactor = 1.0
        ball.name = "Ball"
        ball.size = CGSize(width: 30, height: 30)
        ball.position = CGPoint(x: frame.midX, y: frame.maxY)
        ball.zPosition = ZPositions.ball
        ball.physicsBody = SKPhysicsBody(circleOfRadius: ball.size.width/2)
        ball.physicsBody?.categoryBitMask = PhysicsCategories.ballCategore
        ball.physicsBody?.contactTestBitMask = PhysicsCategories.switchCategory
        ball.physicsBody?.collisionBitMask = PhysicsCategories.none
        addChild(ball)
    }
  
    func turnWheel(){
        if let newState = SwitchSatae(rawValue: switchState.rawValue + 1){
            switchState = newState
        }else{
            switchState = .colorOne
        }
        
        colorSwitch.run(SKAction.rotate(byAngle: .pi/6, duration: 0.25))
    }
    
    func gameOver(){
        UserDefaults.standard.setValue(score, forKey: "RecentScore")
        if score > UserDefaults.standard.integer(forKey: "HighScore"){
            UserDefaults.standard.setValue(score, forKey: "HighScore")
        }        
        let menuScene = MenuScene(size: view!.bounds.size)
        view!.presentScene(menuScene)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        turnWheel()
    }
}


extension GameScene: SKPhysicsContactDelegate{
    // 01
    // 10
    // 11
    func didBegin(_ contact: SKPhysicsContact) {
        let contactMask = contact.bodyA.categoryBitMask | contact.bodyB.categoryBitMask
        
        if contactMask == PhysicsCategories.ballCategore | PhysicsCategories.switchCategory{
            if let ball = contact.bodyA.node?.name == "Ball" ? contact.bodyA.node as? SKSpriteNode : contact.bodyB.node as? SKSpriteNode{
                if currentColorIndex == switchState.rawValue{
                    run(SKAction.playSoundFileNamed("bling", waitForCompletion: false))
                    score += 1
                    updateScoreLabel()
                    ball.run(SKAction.fadeOut(withDuration: 0.25) , completion: {
                        ball.removeFromParent()
                        self.spawnBall()
                    })
                }else{
                    gameOver()
                }
            }
        }
    }
}
